import { getMinLengthValidator, getMaxLengthValidator, isValid, violates } from '../../validation/validators';

describe('Validation methods', () => {
  it('should return the IDs of failed validators', () => {
    const mockValidators = [
      { errorDescription: 'Arbitrary', id: 'invalid', isValid: () => false, options: {} },
    ];

    expect(violates('arbitrary', mockValidators)).toEqual([ 'invalid' ]);
  });

  it('should not return the IDs of successful validators', () => {
    const mockValidators = [
      { errorDescription: 'Arbitrary', id: 'valid', isValid: () => true, options: {} },
    ];

    expect(violates('arbitrary', mockValidators)).toEqual([]);
  });

  it('should return true if all validators were successful', () => {
    const mockValidators = [
      { errorDescription: 'Arbitrary', id: 'valid', isValid: () => true, options: {} },
    ];

    expect(isValid('arbitrary', mockValidators)).toBe(true);
  });

  it('should return false if one validators failed', () => {
    const mockValidators = [
      { errorDescription: 'Arbitrary', id: 'valid', isValid: () => true, options: {} },
      { errorDescription: 'Arbitrary', id: 'invalid', isValid: () => false, options: {} },
    ];

    expect(isValid('arbitrary', mockValidators)).toBe(false);
  });
});

describe('minLength validator', () => {
  it('should succeed if a given array has items', () => {
    expect(getMinLengthValidator().isValid([ 'arbitrary array item' ])).toBe(true);
  });

  it('should not succeed if a given array has no items', () => {
    expect(getMinLengthValidator().isValid([])).toBe(false);
  });

  it('should succeed if a given array has as many items as specified', () => {
    expect(getMinLengthValidator({ minLength: 1 }).isValid([ 'arbitrary array item' ])).toBe(true);
  });

  it('should not succeed if a given array has fewer items than specified', () => {
    expect(getMinLengthValidator({ minLength: 2 }).isValid([ 'arbitrary array item' ])).toBe(false);
  });

  it('should succeed if a given string has characters', () => {
    expect(getMinLengthValidator().isValid('arbitrary string')).toBe(true);
  });

  it('should not succeed if a given string is empty', () => {
    expect(getMinLengthValidator().isValid('')).toBe(false);
  });

  it('should succeed if a given string has as many characters as specified', () => {
    expect(getMinLengthValidator({ minLength: 13 }).isValid('13 characters')).toBe(true);
  });

  it('should not succeed if a given string has fewer characters than specified', () => {
    expect(getMinLengthValidator({ minLength: 14 }).isValid('13 characters')).toBe(false);
  });
});

describe('maxLength validator', () => {
  it('should succeed if a given array has fewer items than allowed', () => {
    expect(getMaxLengthValidator({ maxLength: 2 }).isValid([ 'arbitrary array item' ])).toBe(true);
  });

  it('should not succeed if a given array has more items than allowed', () => {
    expect(getMaxLengthValidator({ maxLength: 0 }).isValid([ 'arbitrary array item' ])).toBe(false);
  });

  it('should succeed if a given array has as many items as specified', () => {
    expect(getMaxLengthValidator({ maxLength: 1 }).isValid([ 'arbitrary array item' ])).toBe(true);
  });

  it('should succeed if a given string has fewer characters than specified', () => {
    expect(getMaxLengthValidator({ maxLength: 42 }).isValid('arbitrary string')).toBe(true);
  });

  it('should not succeed if a given string has more characters than allowed', () => {
    expect(getMaxLengthValidator({ maxLength: 3 }).isValid('arbitrary string longer than 3')).toBe(false);
  });

  it('should succeed if a given string has as many characters as specified', () => {
    expect(getMaxLengthValidator({ maxLength: 29 }).isValid('arbitrary string of length 29')).toBe(true);
  });
});
