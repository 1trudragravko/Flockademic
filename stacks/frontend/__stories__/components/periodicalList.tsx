import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { StaticRouter } from 'react-router-dom';

import { InitialisedPeriodical } from '../../../../lib/interfaces/Periodical';
import { PeriodicalList } from '../../src/components/periodicalList/component';

storiesOf('PeriodicalList', module)
  .add('Zero', () => {
    const periodicals: InitialisedPeriodical[] = [];

    return <StaticRouter><PeriodicalList periodicals={periodicals}/></StaticRouter>;
  })
  .add('One', () => {
    const periodicals: InitialisedPeriodical[] = [
      { identifier: 'arbitrary-identifier', name: 'Some journal' },
    ];

    return <StaticRouter><PeriodicalList periodicals={periodicals}/></StaticRouter>;
  })
  .add('One, incomplete', () => {
    const periodicals = [
      { identifier: 'arbitrary_uuid' },
    ];

    return <StaticRouter><PeriodicalList periodicals={periodicals}/></StaticRouter>;
  })
  .add('Some', () => {
    const periodicals: InitialisedPeriodical[] = [
    { identifier: 'first', name: 'Science' },
    { identifier: 'second', name: 'Notes on Social Agents', headline: 'Representing Humans In Society' },
    { identifier: 'third', name: 'Sample journal of X' },
    // tslint:disable-next-line:max-line-length
    { identifier: 'fourth', name: 'BioBuilder Magazine', description: 'We celebrate the biodesign ideas, specifications and experimental data gathered by our BioBuilder community during workshops, through BioBuilderClubs throughout the country, and in classrooms around the world' },
    {
      description: 'Studies meant to create a carbon-free living environment.',
      headline: 'Heat your home, not the planet',
      identifier: 'fifth',
      name: 'Zero Carbon Housing',
    },
    { identifier: 'arbitrary_uuid' },
  ];

    return <StaticRouter><PeriodicalList periodicals={periodicals}/></StaticRouter>;
  });
