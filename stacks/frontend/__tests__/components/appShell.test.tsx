// react-ga attempts to modify the DOM to add a <script> tag, so skip that
jest.mock('react-ga');

import { AppShell } from '../../src/components/appShell/component';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

it('should match the snapshot', () => {
  process.env.CODE_BRANCH = 'master';
  const shell = shallow(<AppShell/>);

  expect(toJson(shell)).toMatchSnapshot();
});

it('should not display the menu and show a warning message when in maintenance mode', () => {
  process.env.MAINTENANCE_MODE = true as any;

  const shell = shallow(<AppShell/>);

  expect(shell.find('#mainMenu')).not.toExist();
  // tslint:disable-next-line:max-line-length
  expect(
    shell.find('Route[path="/"]')
    .findWhere((routeElement) => typeof routeElement.prop('exact') === 'undefined'),
  ).toExist();

  delete process.env.MAINTENANCE_MODE;
});

it('should expand the menu when the hamburger icon is clicked', () => {
  const shell = shallow(<AppShell/>);

  expect(shell.find('#mainMenu').hasClass('is-active')).toBe(false);
  expect(shell.find('[aria-controls="mainMenu"]').hasClass('is-active')).toBe(false);
  expect(shell.find('[aria-controls="mainMenu"]').prop('aria-expanded')).toBe(false);

  shell.find('[aria-controls="mainMenu"]').simulate('click', { preventDefault: jest.fn() });

  expect(shell.find('#mainMenu').hasClass('is-active')).toBe(true);
  expect(shell.find('[aria-controls="mainMenu"]').hasClass('is-active')).toBe(true);
  expect(shell.find('[aria-controls="mainMenu"]').prop('aria-expanded')).toBe(true);
});

it('should add the appropriate ARAI attributes to the hamburger menu', () => {
  const shell = shallow(<AppShell/>);

  expect(shell.find('[aria-controls="mainMenu"]').prop('aria-label').length).toBeGreaterThan(0);
  expect(shell.find('[aria-controls="mainMenu"]').prop('aria-label'))
    .toBe(shell.find('[aria-controls="mainMenu"]').prop('title'));
  expect(shell.find('[aria-controls="mainMenu"]').prop('aria-haspopup')).toBe(true);
});

it('should display a warning when not browsing the production version', () => {
  process.env.CODE_BRANCH = 'some-branch';
  const shell = shallow(<AppShell/>);

  expect(shell.find('.notification.is-danger')).toExist();
  expect(shell.find('.notification.is-danger').text()).toMatch('meant for testing');
});
