jest.mock('../../src/services/fetchPeriodicals', () => ({
  fetchPeriodicals: jest.fn().mockReturnValue(Promise.resolve([
    {
      datePublished: '2002-02-14T13:37:00.000Z',
      identifier: 'arbitrary_identifier',
      name: 'Arbitrary name',
    },
  ])),
}));

import { fetchPeriodicals } from '../../src/resources/fetchPeriodicals';

const mockContext = {
  database: {} as any,

  body: undefined,
  headers: {},
  method: 'GET' as 'GET',
  params: [ '/' ],
  path: '/',
  query: null,
};

it('should error when no journals could be found', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/fetchPeriodicals');
  mockedPeriodicalService.fetchPeriodicals.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const promise = fetchPeriodicals(mockContext);

  return expect(promise).rejects.toEqual(new Error('Could not fetch journals.'));
});

it('should call the command handler', (done) => {
  const mockedCommandHandler = require.requireMock('../../src/services/fetchPeriodicals');

  const promise = fetchPeriodicals(mockContext);

  setImmediate(() => {
    expect(mockedCommandHandler.fetchPeriodicals.mock.calls.length).toBe(1);
    done();
  });
});
